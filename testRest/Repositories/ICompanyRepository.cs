﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using testRest.Models;
using testRest.ViewModels;

namespace testRest.Repositories
{
    public interface ICompanyRepository
    {
        IEnumerable<CompanyExportModel> GetByName(string name, int page, int pageSize);
        List<Company> SaveAll(List<Company> list);
    }
}