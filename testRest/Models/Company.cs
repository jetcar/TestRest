﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace testRest.Models
{

    public class Company
    {
        public Guid Id { get; set; }
        [Index()]
        [Column("CompanyName",TypeName = "nvarchar")]
        [StringLength(100)]
        public string Name { get; set; }

        public Guid? ParentId { get; set; }
        public virtual Company Parent { get; set; }
        public virtual List<Company> Daughters { get; set; }
        
    }

   

}
