﻿using System.Collections.Generic;
using testRest.Repositories;

namespace testRest.ViewModels
{

    public class CompanyModel
    {
        public string org_name { get; set; }

        public List<CompanyModel> daughters { get; set; }
        
    }
     public class CompanyExportModel
    {
        public string relationship_type { get; set; }

        public string org_name { get; set; }
        
    }

    
}
