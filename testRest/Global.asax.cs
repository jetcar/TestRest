﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using testRest.DbContext;
using testRest.Models;
using testRest.Repositories;
using testRest.ViewModels;

namespace testRest
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            AutoMapperConfig.Initialize();

            var builder = new ContainerBuilder();
            var config = GlobalConfiguration.Configuration;
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<CompanyRepository>().As<ICompanyRepository>().OwnedByLifetimeScope();
            builder.RegisterType<MyDbContext>().As<MyDbContext>().OwnedByLifetimeScope();
            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

       
    }
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<CompanyModel, Company>()
                    .ForMember(s => s.Name, a => a.MapFrom(x => x.org_name))
                    .ForMember(s => s.Daughters, a => a.MapFrom(x => x.daughters))
                    ;

            });
        }
    }
}
