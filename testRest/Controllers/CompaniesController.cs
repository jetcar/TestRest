﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using testRest.Models;
using testRest.Repositories;
using testRest.ViewModels;

namespace testRest.Controllers
{
    public class CompaniesController : ApiController
    {
        private ICompanyRepository _companyRepository;

        public CompaniesController(ICompanyRepository companyRepository)
        {
            _companyRepository = companyRepository;
        }

        // GET api/values
        public IEnumerable<CompanyExportModel> Get(string name, int page = 1, int pageSize = 100)
        {
            if (pageSize > 100)
                pageSize = 100;
            var companies = _companyRepository.GetByName(name, page, pageSize);
            if (!companies.Any())
                return new List<CompanyExportModel>();
            return companies.ToList();
        }




        public void Post([FromBody]CompanyModel company)
        {
            var entity = Mapper.Map<Company>(company);
            var list = GetOneLevelEntitiesWithIDs(entity, null);
            _companyRepository.SaveAll(list);
        }

        private List<Company> GetOneLevelEntitiesWithIDs(Company company, Guid? parentGuid)
        {
            var result = new List<Company>();
            company.Id = Guid.NewGuid();
            company.ParentId = parentGuid;
            result.Add(company);
            foreach (var companyDaughter in company.Daughters)
            {
                result.AddRange(GetOneLevelEntitiesWithIDs(companyDaughter, company.Id));

            }

            return result;
        }
    }


}
