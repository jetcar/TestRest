namespace testRest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class initial2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Companies",
                c => new
                {
                    Id = c.Guid(nullable: false),
                    CompanyName = c.String(maxLength: 100),
                    ParentId = c.Guid(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Companies", t => t.ParentId)
                .Index(t => t.CompanyName)
                .Index(t => t.ParentId);

        }

        public override void Down()
        {
            DropForeignKey("dbo.Companies", "ParentId", "dbo.Companies");
            DropIndex("dbo.Companies", new[] { "ParentId" });
            DropIndex("dbo.Companies", new[] { "CompanyName" });
            DropTable("dbo.Companies");
        }
    }
}
