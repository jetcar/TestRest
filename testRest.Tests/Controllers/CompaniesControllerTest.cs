﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using testRest;
using testRest.Controllers;
using testRest.DbContext;
using testRest.Repositories;
using testRest.ViewModels;

namespace testRest.Tests.Controllers
{
    [TestClass]
    public class CompaniesControllerTest
    {
        [TestInitialize]
        public void Setup()
        {
            AutoMapperConfig.Initialize();
        }
        [TestCleanup]
        public void CleanUp()
        {
            Mapper.Reset();
        }

        [TestMethod]
        public void SaveLoadSmallTest()
        {
            var originalConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            var connectionString = string.Format(originalConnectionString, System.Environment.CurrentDirectory + "\\App_Data\\aspnet-testRest-20180420012033.mdf");
            var db = new MyDbContext(connectionString);

            db.Database.Delete();
            db.Database.Create();


            // Arrange
            CompaniesController controller = new CompaniesController(new CompanyRepository(db));

            var company = new CompanyModel()
            {
                org_name = "Paradise Island",
                daughters = new List<CompanyModel>()
                {
                    new CompanyModel()
                    {
                        org_name = "Banana tree"
                        , daughters = new List<CompanyModel>()
                        {
                            new CompanyModel() {org_name = "Yellow Banana"},
                            new CompanyModel() {org_name = "Brown Banana"},
                            new CompanyModel() {org_name = "Black Banana"},
                        }
                    },
                    new CompanyModel()
                    {
                        org_name = "Big banana tree", daughters = new List<CompanyModel>()
                        {
                            new CompanyModel()
                            {
                                org_name = "Yellow Banana"
                            },
                            new CompanyModel()
                            {
                                org_name = "Brown Banana"
                            },
                            new CompanyModel()
                            {
                                org_name = "Green Banana"
                            },
                            new CompanyModel()
                            {
                                org_name = "Black Banana",
                                daughters = new List<CompanyModel>()
                                {
                                    new CompanyModel()
                                    {
                                        org_name = "Phoneutria Spider"
                                    }
                                }
                            },
                        }
                            
                    },
                    
                }
            };


            // Act
            controller.Post(company);

            // Assert

            var savedCompanies = controller.Get("Black Banana");

            var dbContext = new MyDbContext(connectionString);
            Assert.AreEqual(11, dbContext.Companies.Count());
            Assert.AreEqual(6, savedCompanies.Count());
            Assert.AreEqual(2, savedCompanies.Count(x => x.relationship_type == "parent"));
            Assert.AreEqual(1, savedCompanies.Count(x => x.relationship_type == "daughter"));
            Assert.AreEqual(3, savedCompanies.Count(x => x.relationship_type == "sister"));
            Assert.AreEqual("parent", savedCompanies.First(x => x.org_name == "Banana tree").relationship_type);
            Assert.AreEqual("parent", savedCompanies.First(x => x.org_name == "Big banana tree").relationship_type);
            Assert.AreEqual("sister", savedCompanies.First(x => x.org_name == "Brown Banana").relationship_type);
            Assert.AreEqual("sister", savedCompanies.First(x => x.org_name == "Green Banana").relationship_type);
            Assert.AreEqual("sister", savedCompanies.First(x => x.org_name == "Yellow Banana").relationship_type);
            Assert.AreEqual("daughter", savedCompanies.First(x => x.org_name == "Phoneutria Spider").relationship_type);
        }

        [TestMethod]
        public void SaveLoadbigTest()
        {
            var originalConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            var connectionString = string.Format(originalConnectionString, System.Environment.CurrentDirectory + "\\App_Data\\aspnet-testRest-20180420012033.mdf");
            var db = new MyDbContext(connectionString);

            db.Database.Delete();
            db.Database.Create();

            // Arrange
            CompaniesController controller = new CompaniesController(new CompanyRepository(db));

            var company = new CompanyModel()
            {
                org_name = "parent",
                daughters = new List<CompanyModel>()
            };
            AddChildren(company, 0, -1);

            for (int i = 0; i < 50000; i++)
            {
                AddChildren(company, 0, i);
            }

            // Act
            Stopwatch timer = new Stopwatch();
            timer.Start();
            controller.Post(company);
            timer.Stop();
            // Assert
            Stopwatch timer2 = new Stopwatch();
            timer2.Start();
            controller = new CompaniesController(new CompanyRepository(new MyDbContext(connectionString)));
            var savedCompanies = controller.Get("child40000.10", 1);
            timer2.Stop();
            Console.WriteLine(timer.Elapsed);
            Console.WriteLine(timer2.Elapsed);
            var dbContext = new MyDbContext(connectionString);
            Console.WriteLine(dbContext.Companies.Count());

            Assert.IsTrue(dbContext.Companies.Count() >= 1000000);
            Assert.AreEqual(2, savedCompanies.Count());
        }

        private static List<CompanyModel> AddChildren(CompanyModel company, int i, int i1)
        {
            i++;
            if (i <= 20)
            {
                var model = new CompanyModel() { org_name = "child" + i1 + "." + i, daughters = new List<CompanyModel>() };
                company.daughters.Add(model);
                AddChildren(model, i, i1);
            }
            return company.daughters;
        }
    }
}
