﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using testRest.DbContext;
using testRest.Models;
using testRest.ViewModels;

namespace testRest.Repositories
{
    public class CompanyRepository : ICompanyRepository
    {
        private MyDbContext _context;

        public CompanyRepository(MyDbContext context)
        {
            _context = context;
        }

        public IEnumerable<CompanyExportModel> GetByName(string name, int page, int pageSize)
        {
            var company = _context.Companies.Where(x => x.Name == name);
            if (!company.Any())
                return null;

            return _context.Companies.Where(x => x.Daughters.Any(a => company.Any(c => c.Id == a.Id)))
                .Select(x => new CompanyExportModel() { org_name = x.Name, relationship_type = RelationType.parent.ToString() })
                .Concat(_context.Companies.Where(x => x.ParentId.HasValue && company.Any(c => c.Id == x.ParentId.Value))
                    .Select(x => new CompanyExportModel() { org_name = x.Name, relationship_type = RelationType.daughter.ToString() }))
                .Concat(_context.Companies.Where(x => company.Any(c => c.ParentId == x.ParentId) && !company.Any(c => c.Id == x.Id))
                    .Select(x => new CompanyExportModel() { org_name = x.Name, relationship_type = RelationType.sister.ToString() })).Distinct()
                .OrderBy(x => x.org_name).Skip((page - 1) * pageSize).Take(pageSize);


        }


        public List<Company> SaveAll(List<Company> list)
        {

            _context.BulkInsertAll<Company>(list);

            return list;
        }

        public enum RelationType
        {
            parent,
            sister,
            daughter,

        }
    }
}